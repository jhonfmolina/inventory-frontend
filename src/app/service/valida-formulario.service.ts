import { Injectable } from "@angular/core";
import { AbstractControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { NotificacionService } from "./notificaciones.service";


interface AllValidationErrors {
  control_name: string;
  error_name: string;
  error_value: any;
}

interface FormGroupControls {
  [key: string]: AbstractControl;
}

@Injectable({
  providedIn: 'root'
})

export class ValidaFormularioService{

  private mensaje: NotificacionService;
  constructor(){this.mensaje = new NotificacionService();}

  validacionFormulario(formulario: FormGroup){
    const error = getFormValidationErrors(formulario.controls).shift();
    if(error){
      switch(error.error_name){
        case 'required':
          this.mensaje.mensajeInfo(error.control_name + " es requerido.");
          break;
        case 'email':
          this.mensaje.mensajeInfo(error.control_name + " digitado no tiene formato correcto.");
          break;
        case 'minlength':
          this.mensaje.mensajeInfo(error.control_name + " ingresado(a) debe ser mayor a " + error.error_value.requiredLength + " digitos.");
          break;
        case 'maxlength':
          this.mensaje.mensajeInfo(error.control_name + " ingresado(a) debe ser inferior a " + error.error_value.requiredLength + " digitos.");
          break;
      }
    }else{
      return true
    }
  }

}

function getFormValidationErrors(controls: FormGroupControls): AllValidationErrors[]{
  let errors: AllValidationErrors[] = [];
  Object.keys(controls).forEach(key => {
    const control = controls[ key ];
    if (control instanceof FormGroup) {
      errors = errors.concat(getFormValidationErrors(control.controls));
    }
    const controlErrors: ValidationErrors = controls[ key ].errors;
    if (controlErrors !== null) {
      Object.keys(controlErrors).forEach(keyError => {
        errors.push({
          control_name: key,
          error_name: keyError,
          error_value: controlErrors[ keyError ]
        });
      });
    }
  })
  return errors;
}
  