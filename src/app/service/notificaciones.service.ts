import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class NotificacionService {

  public _mensajeFormIncompleto = "Formulario contiene errores y/o esta incompleto.";
  public _mensajeSinDatos = "Actualmente no tiene datos ingresados.";
  public _msjErrorValorRequerido = "Verificar formulario, campo(s) requerido.";
  public _msjErrorValorInvalido = "Valor ingresado no es valido."

  constructor() { }

  public mensajeSuccess(msj:string){
    this.Toast.fire({icon: "success", title: msj})
  }

  public mensajeError(msj:string){
    this.Toast.fire({icon: "error",title: msj})
  }

  public mensajeInfo(msj:string){
    this.Toast.fire({icon: "info",title: msj})
  }

  private Toast = Swal.mixin({
    toast: true,
    position: 'bottom-start',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });
  
  
}