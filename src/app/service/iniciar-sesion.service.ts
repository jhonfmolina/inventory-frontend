import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: "root"
})

export class IniciarSesionService {


    private header = new HttpHeaders({
        "Access-Control-Allow-Origin'": "*",
        'Content-Type': 'application/json'
      });

    constructor(public http: HttpClient) {
        
    }

    public url: string = environment.URL_BASE;

    

    crearInicioSesion(DataSesion: any) {
        const URL = this.url + `auth/login`;
        return this.http.post(URL, DataSesion, {headers: this.header});
    }

    cerrarSesion() {
     return localStorage.clear();
    }

    setToken(token: any) {
        localStorage.setItem('Token', token);
    }

    getToken() {
      const TOKEN =  localStorage.getItem('Token');
      return TOKEN;
    }

    registrarUsuario(DataRegitro: any) {
        const URL = this.url + `auth/register`;
        return this.http.post(URL, DataRegitro);
    }

}