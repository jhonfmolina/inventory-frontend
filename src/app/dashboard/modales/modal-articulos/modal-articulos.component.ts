import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs/internal/Subscription';
import { IniciarSesionService } from 'src/app/service/iniciar-sesion.service';
import { NotificacionService } from 'src/app/service/notificaciones.service';
import { LoaderService } from 'src/app/shared/loader/servicio/loader.service';
import { environment } from 'src/environments/environment';
import { ArticulosService } from '../../service/articulos.service';

@Component({
  selector: 'app-modal-articulos',
  templateUrl: './modal-articulos.component.html',
  styleUrls: ['./modal-articulos.component.scss']
})
export class ModalArticulosComponent implements OnInit {

  public dataSource = new MatTableDataSource();
  public displayedColumns: string[] = [ 'nombre', 'concepto', 'marca', 'precio', 'seleccion'];
  public cancelarSubcripcion: Subscription[] = [];
  @ViewChild(MatPaginator, { static: false }) set paginator(
    value: MatPaginator
  ) {
    if (this.dataSource) {
      this.dataSource.paginator = value;
    }
  }

  constructor(
    public dialogRef: MatDialogRef<ModalArticulosComponent>,
    public _sesionService: IniciarSesionService,
    public _mensaje: NotificacionService,
    public _loaderService: LoaderService,
    public _articulosService: ArticulosService
  ) { }

  ngOnInit(): void {
    
    setTimeout(() => {
      this.consultaTipoInterface();  
    });
    this.dataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {
    this.cancelarSubcripcion.forEach(subcripcion => subcripcion.unsubscribe());
  }

  consultaTipoInterface() {
    this._loaderService.openLoader();
    this.cancelarSubcripcion.push(this._articulosService.listarArticulos().subscribe((resp:any) => {
      !environment.production? console.log(resp) : false;
      this._loaderService.closeLoader();
      resp.data.length > 0? this.dataSource.data = resp.data: this._mensaje.mensajeInfo(this._mensaje._mensajeSinDatos)
    }))
  }

  filtrar(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  seleccionar(elemento){
    this.dialogRef.close(elemento);
  }

  cerrarFormulario(){
    this.dialogRef.close();
  }
}
