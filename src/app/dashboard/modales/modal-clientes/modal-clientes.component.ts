import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ClientesService } from '../../service/clientes.service';

@Component({
  selector: 'app-modal-clientes',
  templateUrl: './modal-clientes.component.html',
  styleUrls: ['./modal-clientes.component.scss']
})
export class ModalClientesComponent implements OnInit {

  displayedColumns: string[] = ['identificacion', 'nombre', 'contacto', 'seleccion'];
  dataSource = new MatTableDataSource();

  constructor(public _clienteService: ClientesService,
    public dialogRef: MatDialogRef<ModalClientesComponent>,) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.listarClientes();
    });
  }

  listarClientes(){
    this._clienteService.listarClientes().subscribe((resp:any) => {
      console.log(resp);
      this.dataSource.data = resp.data;
    })
  }

  filtrar(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  seleccionar(cliente:any) {
    this.dialogRef.close(cliente);
  }

  cerrarFormulario(){
    this.dialogRef.close();
  }
}
