import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ProveedoresService } from '../../service/proveedores.service';

@Component({
  selector: 'app-modal-proveedores',
  templateUrl: './modal-proveedores.component.html',
  styleUrls: ['./modal-proveedores.component.scss']
})
export class ModalProveedoresComponent implements OnInit {

  displayedColumns: string[] = ['identificacion', 'nombre', 'contacto', 'seleccion'];
  dataSource = new MatTableDataSource();

  constructor(public _proveedoresService: ProveedoresService,
    public dialogRef: MatDialogRef<ModalProveedoresComponent>,) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.listarProveedores();
    });
  }

  listarProveedores(){
    this._proveedoresService.listarProveedores().subscribe((resp:any) => {
      console.log(resp);
      this.dataSource.data = resp.data;
    })
  }

  filtrar(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  seleccionar(proveedor:any) {
    this.dialogRef.close(proveedor);
  }

  cerrarFormulario(){
    this.dialogRef.close();
  }

}
