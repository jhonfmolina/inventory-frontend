import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalProveedoresComponent } from './modal-proveedores/modal-proveedores.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material.module';
import { ModalArticulosComponent } from './modal-articulos/modal-articulos.component';
import { ModalClientesComponent } from './modal-clientes/modal-clientes.component';



@NgModule({
  declarations: [ModalProveedoresComponent, ModalArticulosComponent, ModalClientesComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule
  ]
})
export class ModalesModule { }
