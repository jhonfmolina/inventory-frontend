import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard.routing';
import { ArchivoModule } from './vistas/archivo/archivo.module';
import { ConsultasModule } from './vistas/consultas/consultas.module';
import { MovimientosModule } from './vistas/movimientos/movimientos.module';

@NgModule({
    declarations:[
        DashboardComponent,
    ],
  imports: [
    SharedModule, 
    RouterModule, 
    ReactiveFormsModule, 
    MaterialModule,
    ArchivoModule,
    MovimientosModule,
    ConsultasModule,
    DashboardRoutingModule
],
})
export class DashboardModule {}
