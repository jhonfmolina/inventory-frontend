import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IniciarSesionService } from 'src/app/service/iniciar-sesion.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ArticulosService {
  public url: string = environment.URL_BASE;

  private header = new HttpHeaders({
    "Access-Control-Allow-Origin'": "*",
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this._sesionService.getToken(),
  });

  constructor(
    public http: HttpClient,
    public _sesionService: IniciarSesionService
  ) {}

  obtenerToken() {
    if (this._sesionService.getToken()) {
      this.header;
    }
  }

  crearArticulos(dataArticulos) {
    this.obtenerToken();
    const URL = this.url + `articulos`;
    return this.http.post(URL, dataArticulos, { headers: this.header });
  }

  actualizarArticulos(dataArticulos, identificacion) {
    this.obtenerToken();
    const URL = this.url + `articulos/${identificacion}`;
    return this.http.put(URL, dataArticulos, { headers: this.header });
  }

  listarArticulos() {
    this.obtenerToken();
    const URL = this.url + `articulos`;
    return this.http.get(URL, { headers: this.header });
  }

  buscarArticulos(identificacion) {
    this.obtenerToken();
    const URL = this.url + `articulos/${identificacion}`;
    return this.http.get(URL, { headers: this.header });
  }

}
