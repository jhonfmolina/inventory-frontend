import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IniciarSesionService } from 'src/app/service/iniciar-sesion.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ClientesService {
  public url: string = environment.URL_BASE;

  private header = new HttpHeaders({
    "Access-Control-Allow-Origin'": "*",
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this._sesionService.getToken(),
  });

  constructor(
    public http: HttpClient,
    public _sesionService: IniciarSesionService
  ) {}

  obtenerToken() {
    if (this._sesionService.getToken()) {
      this.header;
    }
  }

  crearClientes(dataCliente) {
    this.obtenerToken();
    const URL = this.url + `clientes`;
    return this.http.post(URL, dataCliente, { headers: this.header });
  }

  actualizarClientes(dataCliente, identificacion) {
    this.obtenerToken();
    const URL = this.url + `clientes/${identificacion}`;
    return this.http.put(URL, dataCliente, { headers: this.header });
  }

  listarClientes() {
    this.obtenerToken();
    const URL = this.url + `clientes`;
    return this.http.get(URL, { headers: this.header });
  }

  buscarClientes(identificacion) {
    this.obtenerToken();
    const URL = this.url + `clientes/${identificacion}`;
    return this.http.get(URL, { headers: this.header });
  }

}
