import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IniciarSesionService } from 'src/app/service/iniciar-sesion.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FacturaComprasService {
  
  public url: string = environment.URL_BASE;

  private header = new HttpHeaders({
    "Access-Control-Allow-Origin'": "*",
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this._sesionService.getToken(),
  });

  constructor(
    public http: HttpClient,
    public _sesionService: IniciarSesionService
  ) {}

  obtenerToken() {
    if (this._sesionService.getToken()) {
      this.header;
    }
  }

  crearFacturaCompras(dataFactura) {
    this.obtenerToken();
    const URL = this.url + `facturas/compras`;
    return this.http.post(URL, dataFactura, { headers: this.header });
  }

}