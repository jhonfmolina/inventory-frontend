import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IniciarSesionService } from 'src/app/service/iniciar-sesion.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProveedoresService {
  public url: string = environment.URL_BASE;

  private header = new HttpHeaders({
    "Access-Control-Allow-Origin'": "*",
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this._sesionService.getToken(),
  });

  constructor(
    public http: HttpClient,
    public _sesionService: IniciarSesionService
  ) {}

  obtenerToken() {
    if (this._sesionService.getToken()) {
      this.header;
    }
  }

  crearProveedores(dataProveedores) {
    this.obtenerToken();
    const URL = this.url + `proveedores`;
    return this.http.post(URL, dataProveedores, { headers: this.header });
  }

  actualizarProveedores(dataProveedores, identificacion) {
    this.obtenerToken();
    const URL = this.url + `proveedores/${identificacion}`;
    return this.http.put(URL, dataProveedores, { headers: this.header });
  }

  listarProveedores() {
    this.obtenerToken();
    const URL = this.url + `proveedores`;
    return this.http.get(URL, { headers: this.header });
  }

  buscarProveedores(identificacion) {
    this.obtenerToken();
    const URL = this.url + `proveedores/${identificacion}`;
    return this.http.get(URL, { headers: this.header });
  }

}