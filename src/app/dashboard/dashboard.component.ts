import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { IniciarSesionService } from '../service/iniciar-sesion.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    public _iniciarSesionService: IniciarSesionService,
    public _router: Router,
    public _tituloService: Title
    ) { }

  ngOnInit(): void {
    this._tituloService.setTitle('Dashboard');
  }


  cerrarSesion() {
    this._iniciarSesionService.cerrarSesion();
    this._router.navigate(['/login']);
  }
}
