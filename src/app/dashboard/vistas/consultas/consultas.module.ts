import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultasRoutingModule } from './consultas-routing.module';
import { FacturaVentasFechaComponent } from './factura-ventas-fecha/factura-ventas-fecha.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';


@NgModule({
  declarations: [FacturaVentasFechaComponent],
  imports: [
    CommonModule,
    SharedModule, 
    RouterModule, 
    ReactiveFormsModule, 
    MaterialModule,
    ConsultasRoutingModule
  ]
})
export class ConsultasModule { }
