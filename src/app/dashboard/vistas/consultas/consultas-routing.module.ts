import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FacturaVentasFechaComponent } from './factura-ventas-fecha/factura-ventas-fecha.component';

const routes: Routes = [
  {path:'fact-ventas-fecha', component: FacturaVentasFechaComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultasRoutingModule { }
