import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FacturaComprasComponent } from './factura-compras/factura-compras.component';
import { FacturaVentasComponent } from './factura-ventas/factura-ventas.component';

const routes: Routes = [
  {path:'factura-ventas', component: FacturaVentasComponent},
  {path:'factura-compras', component: FacturaComprasComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovimientosRoutingModule { }
