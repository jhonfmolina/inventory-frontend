import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Title } from '@angular/platform-browser';

import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { ModalArticulosComponent } from 'src/app/dashboard/modales/modal-articulos/modal-articulos.component';
import { ModalClientesComponent } from 'src/app/dashboard/modales/modal-clientes/modal-clientes.component';
import { FacturaVentasService } from 'src/app/dashboard/service/facturaVentas.service';
import { NotificacionService } from 'src/app/service/notificaciones.service';
import { ValidaFormularioService } from 'src/app/service/valida-formulario.service';
import { LoaderService } from 'src/app/shared/loader/servicio/loader.service';

@Component({
  selector: 'app-factura-ventas',
  templateUrl: './factura-ventas.component.html',
  styleUrls: ['./factura-ventas.component.scss'],
})
export class FacturaVentasComponent implements OnInit {
  formFacturaVenta: FormGroup;
  formFacturaDetalle: FormGroup;
  identificador: string = '';
  detalleFactura: Array<any> = [];
  SUBTOTAL: any = 0;
  DESCUENTO: any = 0;
  IVA: any = 0;
  descuento: any;
  iva: any;
  public dataSource = new MatTableDataSource();
  public displayedColumns: string[] = [
    'codigo',
    'descripcion',
    'cantidad',
    'precio',
    'iva',
    'descuento',
    'subtotal',
    'seleccionar',
  ];
  @ViewChild(MatPaginator, { static: false }) set paginator(
    value: MatPaginator
  ) {
    if (this.dataSource) {
      this.dataSource.paginator = value;
    }
  }

  constructor(
    public dialog: MatDialog,
    public _facturaVentasService: FacturaVentasService,
    public _validarFormulario: ValidaFormularioService,
    public _formBuilder: FormBuilder,
    public _mensaje: NotificacionService,
    public _loader: LoaderService,
    public _tituloService: Title
  ) {
    this.formFacturaVenta = this._formBuilder.group({
      fecha_emision: ['', Validators.required],
      cliente_id: ['', Validators.required],
      cliente: ['', Validators.required],
      valor_total: ['', Validators.required],
      valor_total_iva: ['', Validators.required],
      valor_total_descuento: ['', Validators.required],
    });
    this.formFacturaDetalle = this._formBuilder.group({
      articulo_id: [''],
      articulo: ['', Validators.required],
      concepto_articulo: ['', Validators.required],
      contidad: ['', Validators.required],
      precio_unitario: ['', Validators.required],
      descuento: ['', Validators.required],
      iva: ['', Validators.required],
      sub_total: [''],
    });
  }

  ngOnInit(): void {
    this._tituloService.setTitle('Factura ventas');
    this.dataSource.paginator = this.paginator;
  }

  cancelar() {
    this.dataSource.data = [];
    this.formFacturaVenta.reset();
  }

  formControl = () => this.formFacturaVenta.controls;
  formControlDetalle = () => this.formFacturaDetalle.controls;

  calculos() {
    this.formControlDetalle().sub_total.setValue(
      this.formControlDetalle().contidad.value *
        this.formControlDetalle().precio_unitario.value
    );
    this.descuento =
      this.formControlDetalle().sub_total.value *
      (this.formControlDetalle().descuento.value / 100);
    this.iva =
      (this.formControlDetalle().sub_total.value - this.descuento) *
      (+this.formControlDetalle().iva.value / 100);
  }

  eliminarDetalle(id: number) {
    this.detalleFactura.splice(id, 1);
    this.sumaTotales();
    this.dataSource.data = this.detalleFactura;
  }

  agregarDetalle() {
    if (this._validarFormulario.validacionFormulario(this.formFacturaDetalle)) {
      this.calculos();
      this.detalleFactura.push({
        articulo_id: this.formControlDetalle().articulo_id.value,
        concepto_articulo: this.formControlDetalle().concepto_articulo.value,
        contidad: this.formControlDetalle().contidad.value,
        precio_unitario: this.formControlDetalle().precio_unitario.value,
        descuento: this.descuento,
        iva: this.iva,
        sub_total: this.formControlDetalle().sub_total.value,
      });
      this.formFacturaDetalle.reset();
      this.dataSource.data = this.detalleFactura;
      this.sumaTotales();
    }
  }

  sumaTotales() {
    this.detalleFactura.forEach((item) => {
      this.DESCUENTO += item.descuento;
      this.IVA += item.iva;
      this.SUBTOTAL += item.sub_total;
    });
    this.formControl().valor_total_descuento.setValue(this.DESCUENTO);
    this.formControl().valor_total_iva.setValue(this.IVA);
    this.formControl().valor_total.setValue(
      this.SUBTOTAL - this.DESCUENTO + this.IVA
    );
  }

  dataFacturaVentas = () => ({
    fecha_emision: this.formControl().fecha_emision.value,
    cliente_id: this.formControl().cliente_id.value,
    valor_total: +this.formControl().valor_total.value,
    valor_total_iva: +this.formControl().valor_total_iva.value,
    valor_total_descuento: +this.formControl().valor_total_descuento.value,
    detalle: this.detalleFactura,
  });

  crearFacturaCompras() {
    if (this._validarFormulario.validacionFormulario(this.formFacturaVenta)) {
      this._loader.openLoader();
      this._facturaVentasService
        .crearFacturaVentas(this.dataFacturaVentas())
        .subscribe(
          (resp: any) => {
            console.log(resp);
            this._loader.closeLoader();
            this.formFacturaVenta.reset();
           
            
            confirm('Desea imprimir la factura de venta?')
          ?  this.generatePdf(resp.data)
          : false;
          this._mensaje.mensajeSuccess(
            'Factura de compra creada exitosamente.'
          );
          this.detalleFactura=[];
          this.dataSource.data=this.detalleFactura;
          },
          (error) => {
            this._loader.closeLoader();
            console.log(error);
          }
        );
    }
  }

  abrirModalClientes() {
    const dialogRef = this.dialog.open(ModalClientesComponent, {
      width: '80%',
      height: '300px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.formControl().cliente_id.setValue(result.id);
      this.formControl().cliente.setValue(
        result.nombres + ' ' + result.apellidos
      );
    });
  }

  abrirModalArticulos() {
    const dialogRef = this.dialog.open(ModalArticulosComponent, {
      width: '80%',
      height: '300px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      this.formControlDetalle().articulo_id.setValue(result.id);
      this.formControlDetalle().articulo.setValue(result.nombre);
      this.formControlDetalle().concepto_articulo.setValue(result.descripcion);
    });
  }

  generatePdf(data) {
    (<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;
    const dd: any = {
      pageSize: 'A4',
      pageOrientation: 'landscape',
      pageMargins: [0, 80, 0, 80],

      header: [
        {
          margin: [30, 10, 20, 10],
          columns: [
            {
              text: [
                {
                  text: 'Inventory S.A.S'.toUpperCase() + '\n',
                  bold: true,
                  italics: 'true',
                  fontSize: 14,
                },
                {
                  text:
                    'atcliente@inventory.com' +
                    '\n' +
                    'Barranquilla, Atlántico' +
                    '\n' +
                    '(5) 3856013 - 3869730',
                  italics: 'true',
                  fontSize: 8,
                },
              ],
            },
          ],
        },
      ],
      watermark: {
        text: 'Inventory',
        color: 'f2f2f2',
        opacity: 0.1,
        bold: true,
        italics: true,
      },
      content: [
        {
          margin: [0, 0, 0, 10],
          alignment: 'center',
          text: [
            {
              text: 'Factura de Ventas' + '\n',
              bold: true,
              fontSize: 18,
            },
            {
              text:
                'Fecha de Emisión: ' +
                '2121-08-14' +
                '   ' +
                'Numero: ' +
                'M085' +
                '\n',
              fontSize: 12,
            },
          ],
        },
        {
          margin: [30, 0, 0, 0],
          alignment: 'center',
          table: {
            headerRows: 1,
            widths: [150, 150, 150, 150],
            body: [
              [
                { text: 'Cliente', style: 'tableHeader' },
                { text: 'Telefonos', style: 'tableHeader' },
                { text: 'Dirección', style: 'tableHeader' },
                { text: 'Correo', style: 'tableHeader' },
              ],
              [
                {
                  text: 'Jhon Molina',
                  border: [false, false, false, false],
                  italics: 'true',
                },
                {
                  text: '3213044576',
                  border: [false, false, false, false],
                  italics: 'true',
                },
                {
                  text: 'Carrera 23, 22-51',
                  border: [false, false, false, false],
                  italics: 'true',
                },
                {
                  text: 'jmolina@hotmail.com',
                  border: [false, false, false, false],
                  italics: 'true',
                },
              ],
            ],
          },
          layout: 'headerLineOnly',
        },

        {
          margin: [30, 30, 0, 0],
          text: 'Detalle de la Factura',
          style: 'subheader',
        },
        {
          margin: [30, 10, 0, 0],
          style: 'tableExample',
          table: {
            headerRows: 1,
            widths: [100, 220, 100, 100, 100, 100],
            body: [
              [
                { text: 'Articulo', style: 'tableHeader' },
                { text: 'Descripción', style: 'tableHeader' },
                { text: 'Cantidad', style: 'tableHeader' },
                { text: 'Precio Unitario', style: 'tableHeader' },
                { text: 'IVA', style: 'tableHeader' },
                { text: 'Descuento', style: 'tableHeader' },
              ],
              ['', '', '', '', '', ''],
            ],
          },
        },
      ],
      footer: {
        columns: [
          {
            text: 'Documento Impreso por: Inventory S.A.S',
            alignment: 'center',
          },
        ],
      },
    };
    pdfMake.createPdf(dd).open();
  }
}
