import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovimientosRoutingModule } from './movimientos-routing.module';
import { FacturaVentasComponent } from './factura-ventas/factura-ventas.component';
import { FacturaComprasComponent } from './factura-compras/factura-compras.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { ModalesModule } from '../../modales/modales.module';


@NgModule({
  declarations: [FacturaVentasComponent, FacturaComprasComponent],
  imports: [
    CommonModule,
    SharedModule, 
    RouterModule, 
    ReactiveFormsModule, 
    MaterialModule,
    ModalesModule,
    MovimientosRoutingModule
  ]
})
export class MovimientosModule { }
