import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Title } from '@angular/platform-browser';
import { ArticulosService } from 'src/app/dashboard/service/articulos.service';
import { NotificacionService } from 'src/app/service/notificaciones.service';
import { ValidaFormularioService } from 'src/app/service/valida-formulario.service';
import { LoaderService } from 'src/app/shared/loader/servicio/loader.service';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.scss'],
})
export class ArticulosComponent implements OnInit {
  formArticulos: FormGroup;
  estadoBtn: any;
  identificador: string = '';
  public dataSource = new MatTableDataSource();
  public displayedColumns: string[] = [
    'codigo',
    'nombre',
    'descripcion',
    'precio_costo',
    'seleccionar',
  ];
  @ViewChild(MatPaginator, { static: false }) set paginator(
    value: MatPaginator
  ) {
    if (this.dataSource) {
      this.dataSource.paginator = value;
    }
  }

  constructor(
    public _articulosService: ArticulosService,
    public _validarFormulario: ValidaFormularioService,
    public _formBuilder: FormBuilder,
    public _mensaje: NotificacionService,
    public _loader: LoaderService,
    public _tituloService: Title
  ) {
    this.formArticulos = this._formBuilder.group({
      nombre: ['', Validators.required],
      marca: ['', Validators.required],
      codigo: ['', Validators.required],
      descripcion: ['', Validators.required],
      precio_costo: ['', Validators.required],
      iva: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this._tituloService.setTitle('Productos');
    this.setEstadoInicialBtn();
    this.dataSource.paginator = this.paginator;
  }

  cancelar() {
    this.dataSource.data = [];
    this.setEstadoInicialBtn();
  }

  setEstadoInicialBtn() {
    this.estadoBtn = {
      btnCrear: false,
      btnGuardar: true,
      btnActualizar: false,
      btnCancelar: true,
    };
    this.formArticulos.reset();
  }

  formControl = () => this.formArticulos.controls;

  dataCrearProducto = () => ({
    nombre: this.formControl().nombre.value,
    marca: this.formControl().marca.value,
    codigo: this.formControl().codigo.value,
    descripcion: this.formControl().descripcion.value,
    precio_costo: this.formControl().precio_costo.value,
    iva: this.formControl().iva.value,
  });

  dataActualizarProducto = () => ({
    nombre: this.formControl().nombre.value,
    marca: this.formControl().marca.value,
    codigo: this.formControl().codigo.value,
    descripcion: this.formControl().descripcion.value,
    precio_costo: this.formControl().precio_costo.value,
    iva: this.formControl().iva.value,
  });

  crearProducto() {
    if (this._validarFormulario.validacionFormulario(this.formArticulos)) {
      this._loader.openLoader();
      this._articulosService.crearArticulos(this.dataCrearProducto()).subscribe(
        (resp) => {
          console.log(resp);
          this._loader.closeLoader();
          this._mensaje.mensajeSuccess('Producto creado exitosamente.');
          this.formArticulos.reset();
        },
        (error) => {
          this._loader.closeLoader();
          console.log(error);
          this._mensaje.mensajeError('Producto ya esta registrado.');
        }
      );
    }
  }

  actualizarProducto() {
    if (this._validarFormulario.validacionFormulario(this.formArticulos)) {
      this._loader.openLoader();
      this._articulosService
        .actualizarArticulos(this.dataCrearProducto(), this.identificador)
        .subscribe(
          (resp) => {
            console.log(resp);
            this._loader.closeLoader();
            this._mensaje.mensajeSuccess('Producto actualizado exitosamente.');
            this.formArticulos.reset();
            this.dataSource.data = [];
          },
          (error) => {
            this._loader.closeLoader();
            console.log(error);
          }
        );
    }
  }

  listarProducto() {
    this._loader.openLoader();
    this._articulosService.listarArticulos().subscribe(
      (resp: any) => {
        if (resp.data.length > 0) {
          this.dataSource.data = resp.data;
          console.log(resp);
        } else {
          this._mensaje.mensajeInfo('Aun no cuenta con productos registrados.');
        }
        this._loader.closeLoader();
      },
      (error) => {
        console.log(error);
        this._loader.closeLoader();
      }
    );
  }

  seleccionarProducto(idProducto) {
    this.formArticulos.reset();
    this._loader.openLoader();
    this._articulosService.buscarArticulos(idProducto.id).subscribe(
      (resp: any) => {
        console.log(resp);
        this._loader.closeLoader();
        this.identificador = resp.data.id;
        this.formControl().nombre.setValue(resp.data.nombre);
        this.formControl().marca.setValue(resp.data.marca);
        this.formControl().codigo.setValue(resp.data.codigo);
        this.formControl().descripcion.setValue(resp.data.descripcion);
        this.formControl().precio_costo.setValue(resp.data.precio_costo);
        this.formControl().iva.setValue(resp.data.iva);
      },
      (error) => {
        console.log(error);
        this._loader.closeLoader();
      }
    );
    this.estadoBtn = {
      btnCrear: false,
      btnGuardar: false,
      btnActualizar: true,
      btnCancelar: true,
    };
  }
}
