import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Title } from '@angular/platform-browser';
import { ProveedoresService } from 'src/app/dashboard/service/proveedores.service';
import { NotificacionService } from 'src/app/service/notificaciones.service';
import { ValidaFormularioService } from 'src/app/service/valida-formulario.service';
import { LoaderService } from 'src/app/shared/loader/servicio/loader.service';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.scss']
})
export class ProveedoresComponent implements OnInit {

  formProveedor: FormGroup;
  estadoBtn: any;
  identificador: string = '';
  public dataSource = new MatTableDataSource();
  public displayedColumns: string[] = [
    'nombres',
    'correo',
    'contacto',
    'direccion',
    'seleccionar',
  ];
  @ViewChild(MatPaginator, { static: false }) set paginator(
    value: MatPaginator
  ) {
    if (this.dataSource) {
      this.dataSource.paginator = value;
    }
  }

  constructor(
    public _proveedorService: ProveedoresService,
    public _validarFormulario: ValidaFormularioService,
    public _formBuilder: FormBuilder,
    public _mensaje: NotificacionService,
    public _loader: LoaderService,
    public _tituloService: Title
  ) {
    this.formProveedor = this._formBuilder.group({
      tipo_documento: ['', Validators.required],
      documento: ['', Validators.required],
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      correo: ['', [Validators.required, Validators.email]],
      contacto: ['', Validators.required],
      direccion: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this._tituloService.setTitle('Proveedores');
    this.setEstadoInicialBtn();
    this.dataSource.paginator = this.paginator;
  }

  cancelar() {
    this.dataSource.data = [];
    this.setEstadoInicialBtn();
  }

  setEstadoInicialBtn() {
    this.estadoBtn = {
      btnCrear: false,
      btnGuardar: true,
      btnActualizar: false,
      btnCancelar: true,
    };
    this.formProveedor.reset();
  }

  formControl = () => this.formProveedor.controls;

  dataCrearProveedores = () => ({
    nombres: this.formControl().nombre.value,
    apellidos: this.formControl().apellidos.value,
    tipo_documento: this.formControl().tipo_documento.value,
    documento: this.formControl().documento.value,
    correo: this.formControl().correo.value,
    contacto: this.formControl().contacto.value,
    direccion: this.formControl().direccion.value,
  });

  dataActualizarProveedores = () => ({
    nombres: this.formControl().nombre.value,
    apellidos: this.formControl().apellidos.value,
    tipo_documento: this.formControl().tipo_documento.value,
    documento: this.formControl().documento.value,
    correo: this.formControl().correo.value,
    contacto: this.formControl().contacto.value,
    direccion: this.formControl().direccion.value,
  });

  crearProveedores() {
    if (this._validarFormulario.validacionFormulario(this.formProveedor)) {
      this._loader.openLoader();
      this._proveedorService.crearProveedores(this.dataCrearProveedores()).subscribe(
        (resp) => {
          console.log(resp);
          this._loader.closeLoader();
          this._mensaje.mensajeSuccess('Proveedor creado exitosamente.');
          this.formProveedor.reset();
          this.dataSource.data = [];
        },
        (error) => {
          this._loader.closeLoader();
          console.log(error);
          this._mensaje.mensajeError('Proveedor ya esta registrado.');
        }
      );
    }
  }

  actualizarProveedores() {
    if (this._validarFormulario.validacionFormulario(this.formProveedor)) {
      this._loader.openLoader();
      this._proveedorService.actualizarProveedores(this.dataCrearProveedores(), this.identificador).subscribe(
        (resp) => {
          console.log(resp);
          this._loader.closeLoader();
          this._mensaje.mensajeSuccess('Proveedor actualizado exitosamente.');
          this.formProveedor.reset();
          this.dataSource.data = [];
        },
        (error) => {
          this._loader.closeLoader();
          console.log(error);
        }
      );
    }
  }

  listarProveedores() {
    this._loader.openLoader();
    this._proveedorService.listarProveedores().subscribe(
      (resp: any) => {
        if (resp.data.length > 0) {
          this.dataSource.data = resp.data;
          console.log(resp);
        }else {
          this._mensaje.mensajeInfo('Aun no cuenta con Proveedores registrados.');
        }
        this._loader.closeLoader();
      },
      (error) => {
        console.log(error);
        this._loader.closeLoader();
      }
    );
  }

  seleccionarProveedores(idProveedor) {
    this.formProveedor.reset();
    this._loader.openLoader();
    this._proveedorService.buscarProveedores(idProveedor.id).subscribe(
      (resp: any) => {
        console.log(resp);
        this._loader.closeLoader();
        this.identificador = resp.data.id;
        this.formControl().nombre.setValue(resp.data.nombres);
        this.formControl().apellidos.setValue(resp.data.apellidos);
        this.formControl().tipo_documento.setValue(resp.data.tipo_documento);
        this.formControl().documento.setValue(resp.data.documento);
        this.formControl().correo.setValue(resp.data.correo);
        this.formControl().contacto.setValue(resp.data.contacto);
        this.formControl().direccion.setValue(resp.data.direccion);
      },
      (error) => {
        console.log(error);
        this._loader.closeLoader();
      }
    );
    this.estadoBtn = {
      btnCrear: false,
      btnGuardar: false,
      btnActualizar: true,
      btnCancelar: true,
    };
  }

}
