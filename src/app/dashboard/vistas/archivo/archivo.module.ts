import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArchivoRoutingModule } from './archivo-routing.module';
import { ClientesComponent } from './clientes/clientes.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { ArticulosComponent } from './articulos/articulos.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';


@NgModule({
  declarations: [ClientesComponent, ProveedoresComponent, ArticulosComponent],
  imports: [
    CommonModule,
    SharedModule, 
    RouterModule, 
    ReactiveFormsModule, 
    MaterialModule,
    ArchivoRoutingModule
  ]
})
export class ArchivoModule { }
