import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Title } from '@angular/platform-browser';
import { ClientesService } from 'src/app/dashboard/service/clientes.service';
import { NotificacionService } from 'src/app/service/notificaciones.service';
import { ValidaFormularioService } from 'src/app/service/valida-formulario.service';
import { LoaderService } from 'src/app/shared/loader/servicio/loader.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss'],
})
export class ClientesComponent implements OnInit {
  formCliente: FormGroup;
  estadoBtn: any;
  identificador: string = '';
  public dataSource = new MatTableDataSource();
  public displayedColumns: string[] = [
    'nombres',
    'correo',
    'contacto',
    'direccion',
    'seleccionar',
  ];
  @ViewChild(MatPaginator, { static: false }) set paginator(
    value: MatPaginator
  ) {
    if (this.dataSource) {
      this.dataSource.paginator = value;
    }
  }

  constructor(
    public _clienteService: ClientesService,
    public _validarFormulario: ValidaFormularioService,
    public _formBuilder: FormBuilder,
    public _mensaje: NotificacionService,
    public _loader: LoaderService,
    public _tituloService: Title
  ) {
    this.formCliente = this._formBuilder.group({
      tipo_documento: ['', Validators.required],
      documento: ['', Validators.required],
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      correo: ['', [Validators.required, Validators.email]],
      contacto: ['', Validators.required],
      direccion: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this._tituloService.setTitle('Clientes');
    this.setEstadoInicialBtn();
    this.dataSource.paginator = this.paginator;
  }

  cancelar() {
    this.dataSource.data = [];
    this.setEstadoInicialBtn();
  }

  setEstadoInicialBtn() {
    this.estadoBtn = {
      btnCrear: false,
      btnGuardar: true,
      btnActualizar: false,
      btnCancelar: true,
    };
    this.formCliente.reset();
  }

  formControl = () => this.formCliente.controls;

  dataCrearCliente = () => ({
    nombres: this.formControl().nombre.value,
    apellidos: this.formControl().apellidos.value,
    tipo_documento: this.formControl().tipo_documento.value,
    documento: this.formControl().documento.value,
    correo: this.formControl().correo.value,
    contacto: this.formControl().contacto.value,
    direccion: this.formControl().direccion.value,
  });

  dataActualizarCliente = () => ({
    nombres: this.formControl().nombre.value,
    apellidos: this.formControl().apellidos.value,
    tipo_documento: this.formControl().tipo_documento.value,
    documento: this.formControl().documento.value,
    correo: this.formControl().correo.value,
    contacto: this.formControl().contacto.value,
    direccion: this.formControl().direccion.value,
  });

  crearClientes() {
    if (this._validarFormulario.validacionFormulario(this.formCliente)) {
      this._loader.openLoader();
      this._clienteService.crearClientes(this.dataCrearCliente()).subscribe(
        (resp) => {
          console.log(resp);
          this._loader.closeLoader();
          this._mensaje.mensajeSuccess('Cliente creado exitosamente.');
          this.formCliente.reset();
        },
        (error) => {
          this._loader.closeLoader();
          console.log(error);
          this._mensaje.mensajeError('Cliente ya esta registrado.');
        }
      );
    }
  }

  actualizarClientes() {
    if (this._validarFormulario.validacionFormulario(this.formCliente)) {
      this._loader.openLoader();
      this._clienteService.actualizarClientes(this.dataCrearCliente(), this.identificador).subscribe(
        (resp) => {
          console.log(resp);
          this._loader.closeLoader();
          this._mensaje.mensajeSuccess('Cliente actualizado exitosamente.');
          this.formCliente.reset();
          this.dataSource.data = [];
        },
        (error) => {
          this._loader.closeLoader();
          console.log(error);
        }
      );
    }
  }

  listarClientes() {
    this._loader.openLoader();
    this._clienteService.listarClientes().subscribe(
      (resp: any) => {
        if (resp.data.length > 0) {
          this.dataSource.data = resp.data;
          console.log(resp);
        }else {
          this._mensaje.mensajeInfo('Aun no cuenta con Clientes registrados.');
        }
        this._loader.closeLoader();
      },
      (error) => {
        console.log(error);
        this._loader.closeLoader();
      }
    );
  }

  seleccionarClientes(idCliente) {
    this.formCliente.reset();
    this._loader.openLoader();
    this._clienteService.buscarClientes(idCliente.id).subscribe(
      (resp: any) => {
        console.log(resp);
        this._loader.closeLoader();
        this.identificador = resp.data.id;
        this.formControl().nombre.setValue(resp.data.nombres);
        this.formControl().apellidos.setValue(resp.data.apellidos);
        this.formControl().tipo_documento.setValue(resp.data.tipo_documento);
        this.formControl().documento.setValue(resp.data.documento);
        this.formControl().correo.setValue(resp.data.correo);
        this.formControl().contacto.setValue(resp.data.contacto);
        this.formControl().direccion.setValue(resp.data.direccion);
      },
      (error) => {
        console.log(error);
        this._loader.closeLoader();
      }
    );
    this.estadoBtn = {
      btnCrear: false,
      btnGuardar: false,
      btnActualizar: true,
      btnCancelar: true,
    };
  }
}
