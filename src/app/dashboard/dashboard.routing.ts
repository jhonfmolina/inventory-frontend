import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//COMPONENTE PRINCIPAL DASHBOARD
import { DashboardComponent } from './dashboard.component';

const rutas: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'archivo',
        loadChildren: () =>
          import('./vistas/archivo/archivo-routing.module').then(
            (m) => m.ArchivoRoutingModule
          ),
      },
      {
        path: 'movimientos',
        loadChildren: () =>
          import('./vistas/movimientos/movimientos-routing.module').then(
            (m) => m.MovimientosRoutingModule
          ),
      },
      {
        path: 'consultas',
        loadChildren: () =>
          import('./vistas/consultas/consultas-routing.module').then(
            (m) => m.ConsultasRoutingModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(rutas)],
  exports: [RouterModule],
})

export class DashboardRoutingModule {}