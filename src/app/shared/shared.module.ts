import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoaderComponent } from './loader/loader.component';
import { NavegationComponent } from './navegation/navegation.component';
import { LoaderModule } from './loader/loader.module';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    LoaderComponent,
    NavegationComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    LoaderModule
  ], 
  exports: [
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    LoaderComponent,
    NavegationComponent
  ]
})
export class SharedModule { }
