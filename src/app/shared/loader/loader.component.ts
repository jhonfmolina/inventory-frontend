import { Component, OnInit } from '@angular/core';
import { LoaderService } from './servicio/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  public estado : boolean = false;
  public msj!: string;
  constructor( public _loader : LoaderService) { }

  ngOnInit(): void {
    this._loader.estado.subscribe(
      (data:any) => {
        this.msj = "Cargando...";
        this.estado = data;
      }
    )
  }

}
