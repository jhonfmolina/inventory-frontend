import { NgModule } from '@angular/core';
import { LoaderService } from './servicio/loader.service';

@NgModule({
  declarations: [],
  providers: [
    LoaderService
  ],
  imports: []
})
export class LoaderModule { }