import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public estado: EventEmitter<boolean> = new EventEmitter();
  constructor() { }

  openLoader(){
    this.estado.emit(true);
  }

  closeLoader(){
    this.estado.emit(false)
  }
}
