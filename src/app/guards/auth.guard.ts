import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { IniciarSesionService } from '../service/iniciar-sesion.service';
import { Router } from '@angular/router';
import { NotificacionService } from '../service/notificaciones.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    public _iniciarSesionService: IniciarSesionService, 
    public _router: Router,
    public _mensaje: NotificacionService,
    ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ){
    if (this._iniciarSesionService.getToken()) {
      return true;
    } else {
      this._mensaje.mensajeInfo('Debes autenticarte primeramente.');
      return this._router.navigate(['/login']);
    }
  }
}
