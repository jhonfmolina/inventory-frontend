import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { IniciarSesionService } from 'src/app/service/iniciar-sesion.service';
import { NotificacionService } from 'src/app/service/notificaciones.service';
import { LoaderService } from 'src/app/shared/loader/servicio/loader.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  formRegister: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public _iniciarSesionService: IniciarSesionService,
    public _loader: LoaderService,
    public _mensaje: NotificacionService,
    public _router: Router,
    public _tituloService : Title
  ) {
    this.formRegister = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password_confirmation: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this._tituloService.setTitle('Registro de usuarios');
  }

  registrarUsuario() {
    
    if (this.formRegister.controls.email.errors?.email) {
      this._mensaje.mensajeError('Formato de correo electronico incorrecto.');
      return
    }
    if (this.formRegister.controls.password.errors?.minlength) {
      this._mensaje.mensajeError('La contraseña debe ser de minimo 6 caractares.');
      return
    }
    if (this.formRegister.valid) {
      this._loader.openLoader();

      this._iniciarSesionService
          .registrarUsuario(this.formRegister.value).subscribe(
            (resp: any) => {
              console.log(resp);
              this._loader.closeLoader();
              this.formRegister.reset();
              this._mensaje.mensajeSuccess('Te has registrado exitosamente.');
              this._router.navigate(['/login']);
            },
            (error) => {
              this._loader.closeLoader();
              error.error.errors.password? this._mensaje.mensajeError("Las contraseñas no coinciden."): false
            }
          );
    } else {
      this._mensaje.mensajeError(this._mensaje._msjErrorValorRequerido);
    }
  }
}
