import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { IniciarSesionService } from 'src/app/service/iniciar-sesion.service';
import { NotificacionService } from 'src/app/service/notificaciones.service';
import { LoaderService } from 'src/app/shared/loader/servicio/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formLogin : FormGroup;

  

  constructor(
    public formBuilder: FormBuilder,
    public _iniciarSesionService: IniciarSesionService,
    public _mensaje: NotificacionService,
    public _loader: LoaderService,
    public _router: Router,
    public _tituloService: Title
    ) {
      this.formLogin = this.formBuilder.group({
        email: ["", Validators.required],
        password:["", Validators.required]
      })
     }

  ngOnInit(): void {
    this._tituloService.setTitle('Iniciar sesión.');
  }

  iniciarSesion() {
    if (this.formLogin.valid) {  
      this._loader.openLoader();
      this._iniciarSesionService.crearInicioSesion(this.formLogin.value).subscribe((resp:any) => {
        this._iniciarSesionService.setToken(resp.data.token);
        this._loader.closeLoader();
        this._router.navigate(['/dashboard']);
        setTimeout(() => {
          this._mensaje.mensajeSuccess("Bienvenido a Inventory.")
        }, 800);
      }, error => {
        console.log(error.error);
        
        this._loader.closeLoader();
        error.error? this._mensaje.mensajeError("Credenciales invalidas, confirmar correo y/o contraseña."):false;       
      })
    } else {
      this._mensaje.mensajeError(this._mensaje._msjErrorValorRequerido);
    }
  }
}
